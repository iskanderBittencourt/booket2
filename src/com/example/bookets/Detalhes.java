package com.example.bookets;

import java.io.ByteArrayInputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Detalhes extends Activity {
	
	public Cursor rs;
	public SQLiteDatabase bd;
	ImageView foto;
	TextView titulo;
	TextView sub;
	TextView autor;
	TextView isbn;
	TextView categoria;
	TextView editora;
	TextView edicao;
	TextView ano;
	TextView pags;
	Button comprar;
	Button sair;
	public AlertDialog.Builder msgBox;
	public Typeface font;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalhes);
		
		Intent it = getIntent();
		int cod = it.getIntExtra("cod",0);
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		rs = bd.rawQuery("SELECT * FROM livros WHERE cod=?", new String[]{String.valueOf(cod)});
		
		foto = (ImageView)findViewById(R.id.img);
		titulo = (TextView)findViewById(R.id.titulo);
		sub= (TextView)findViewById(R.id.sub);
		autor =(TextView)findViewById(R.id.autor);
		isbn= (TextView)findViewById(R.id.isbn);
		categoria= (TextView)findViewById(R.id.categoria);
		editora= (TextView)findViewById(R.id.editora);
		edicao= (TextView)findViewById(R.id.edicao);
		ano= (TextView)findViewById(R.id.ano);
		pags= (TextView)findViewById(R.id.pags);
		comprar= (Button)findViewById(R.id.comprar);
		sair= (Button)findViewById(R.id.sair);
		font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		
		titulo.setTypeface(font);
		sub.setTypeface(font);
		autor.setTypeface(font);
		isbn.setTypeface(font);
		categoria.setTypeface(font);
		editora.setTypeface(font);
		edicao.setTypeface(font);
		ano.setTypeface(font);
		pags.setTypeface(font);
		comprar.setTypeface(font);
		sair.setTypeface(font);
		
		if(rs.moveToNext()){
			String t = rs.getString(rs.getColumnIndex("titulo"));
			String s = rs.getString(rs.getColumnIndex("subTitulo"));
			String aut = rs.getString(rs.getColumnIndex("autor"));
			String i = rs.getString(rs.getColumnIndex("codIsbn"));
			String c = rs.getString(rs.getColumnIndex("categoria"));
			String e = rs.getString(rs.getColumnIndex("editora"));
			String edica = rs.getString(rs.getColumnIndex("edicao"));
			int an = rs.getInt(rs.getColumnIndex("anoPublic"));
			int p = rs.getInt(rs.getColumnIndex("noPaginas"));
			byte[] image = rs.getBlob(rs.getColumnIndex("foto"));
			
			titulo.setText(t);
			sub.setText(s);
			autor.setText("Autor : " +aut);
			isbn.setText("Cod. ISBN : " +i);
			categoria.setText("Categoria : " +c);
			editora.setText("Editora : " +e);
			edicao.setText("Edi��o : " +edica);
			ano.setText("Ano de publica��o : " +String.valueOf(an));
			pags.setText("N� de p�ginas : " +String.valueOf(p));
			
			ByteArrayInputStream imageStream = new ByteArrayInputStream(image);
	        Bitmap imageBitmap= BitmapFactory.decodeStream(imageStream);
	        foto.setImageBitmap(imageBitmap);
	        
		}
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
		comprar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	msgBox = new AlertDialog.Builder(Detalhes.this);
	        	LayoutInflater factory = LayoutInflater.from(Detalhes.this);
	        	final View view = factory.inflate(R.layout.nunca, null);
	        	msgBox.setView(view);
	        	msgBox.setNeutralButton("Estou muito triste ;-;", new DialogInterface.OnClickListener() {
	        	    public void onClick(DialogInterface dlg, int sumthin) {

	        	    }
	        	});

	        	msgBox.show();
	        }
	    });
	}
}
