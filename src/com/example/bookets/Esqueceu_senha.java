package com.example.bookets;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Esqueceu_senha extends Activity {
	
	private EditText login;
	private EditText senha;
	private EditText confSenha;
	private Button sair;
	private Button confirmar;
	public SQLiteDatabase bd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_esqueceu_senha);
		Intent it= getIntent();
		String l = it.getStringExtra("login");
		
		login = (EditText) findViewById(R.id.login);
		senha = (EditText) findViewById(R.id.senha);
		confSenha = (EditText) findViewById(R.id.confSenha);
		sair = (Button) findViewById(R.id.sair);
		confirmar = (Button) findViewById(R.id.confirmar);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		
		login.setTypeface(font);
		senha.setTypeface(font);
		confSenha.setTypeface(font);
		confirmar.setTypeface(font);
		sair.setTypeface(font);
		
		login.setText(l);
		
		confirmar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(TextUtils.isEmpty(senha.getText().toString())) {
	        		senha.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(confSenha.getText().toString())){
	        		confSenha.setError("Preencha este campo");
	        		return;
	        	}else if(!senha.getText().toString().equals(confSenha.getText().toString())){
	        		Toast.makeText(Esqueceu_senha.this, "Senhas n�o conferem", Toast.LENGTH_LONG).show();
	        	}else{
	        		String l = login.getText().toString();
					String s =  senha.getText().toString();
					bd.execSQL("UPDATE users SET senha=? WHERE login=?",new String[]{s, l});
	        		Intent it = new Intent(Esqueceu_senha.this,Cadastro.class);
		            startActivity(it);
	        	}      
	        }
	    });
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}
}
