package com.example.bookets;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class Entrada_menu extends Activity {
	
	public ListView menu;
	public ArrayList<MenuOptions> menuList;
	public MenuAdapter adapter;
	
	String l;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entrada_menu);
		
		Intent it = getIntent();
		l = it.getStringExtra("login");
		
		menu = (ListView) findViewById(R.id.menu);
		
		menuList = new ArrayList<MenuOptions>();
		menuList.add(new MenuOptions("1","Catalogo de livros", R.drawable.ic_book));
		menuList.add(new MenuOptions("2","Editar Usu�rio", R.drawable.ic_edit));
		menuList.add(new MenuOptions("3","Sobre n�s", R.drawable.ic_person));
		menuList.add(new MenuOptions("4","Exit", R.drawable.ic_exit));
		
		adapter = new MenuAdapter(this, menuList);
		menu.setAdapter(adapter);
		menu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {  		
            	if(position==0){
    				Intent it = new Intent(Entrada_menu.this,Catalogo.class);
    				startActivity(it);
	    		}else if(position==1){
	    			Intent it = new Intent(Entrada_menu.this,EditUser.class);
	    			it.putExtra("login", l);
    				startActivity(it);
	    		}else if(position==2){
	    			Intent it = new Intent(Entrada_menu.this,Equipe.class);
    				startActivity(it);	
	    		}else{
	    			finish();
	    		}
            }
        });
	}
}
