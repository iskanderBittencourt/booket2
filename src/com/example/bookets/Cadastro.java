package com.example.bookets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Cadastro extends Activity {
	
	private EditText nome;
	private EditText login;
	private EditText senha;
	private Button sair;
	private Button confirmar;
	private SQLiteDatabase bd;
	public AlertDialog.Builder msgBox;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);
		
		nome = (EditText) findViewById(R.id.nome);
		login = (EditText) findViewById(R.id.login);
		senha = (EditText) findViewById(R.id.senha);
		sair = (Button) findViewById(R.id.sair);
		confirmar = (Button) findViewById(R.id.confirmar);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		Intent it = getIntent();
		String l = it.getStringExtra("login");
		String s = it.getStringExtra("senha");
		
		
		nome.setTypeface(font);
		login.setTypeface(font);
		senha.setTypeface(font);
		confirmar.setTypeface(font);
		sair.setTypeface(font);
		login.setText(l);
		senha.setText(s);
		
		confirmar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	    		String nomeReg = nome.getText().toString();
	    		String loginReg = login.getText().toString();
	    		String senhaReg = senha.getText().toString();
	    		
	    		Cursor rs = bd.rawQuery("SELECT * FROM users WHERE login=?", new String[]{loginReg});
	          	
	        	if(rs.moveToNext()){
	        		Toast.makeText(Cadastro.this, "Usu�rio j� existente" , Toast.LENGTH_SHORT).show();
	        	}else{
	        		addUser(bd,nomeReg,loginReg,senhaReg,0);
	        		confirmacao(); 
	        	}
	        }
	    });
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}
	
	public void confirmacao(){
		msgBox = new AlertDialog.Builder(this);
		msgBox.setTitle("Cadastro concluido");
		msgBox.setIcon(android.R.drawable.ic_menu_info_details);
		msgBox.setMessage("Entrar?");
		msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent it = new Intent(Cadastro.this,Entrada_menu.class);
	            startActivity(it);	
			}
		});
		msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		msgBox.show();
	}
	public static void addUser(SQLiteDatabase db, String nome, String login, String senha, int tipo){
		ContentValues values = new ContentValues();
		values = new ContentValues();

		values.put("nome", nome);
		values.put("login", login);
		values.put("senha", senha);
		values.put("tipo", tipo);
		db.insert("users", "nome", values);
	}
}
