package com.example.bookets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

public class EditLivro extends Activity implements OnItemSelectedListener{
	
	public static final int IMAGEM_INTERNA = 4;
	public static final int GALLERY_INTENT_REQUEST_CODE = 0x000005;
	public EditText isbn;
	public EditText titulo;
	public EditText subtitulo;
	public EditText edicao;
	public EditText autor;
	public EditText npag;
	public EditText ano;
	public EditText editora;
	public Button sair;
	public Button salvar;
	public Button altImg;
	public Spinner categoria;
	private SQLiteDatabase bd;
	public String cat;
	public AlertDialog.Builder msgBox;
	private Bitmap bitmap;
	public Typeface font;
	public Cursor rs;
	int cod;
	int i=0;
	byte[] imagem;
	ImageView foto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_livro);
		
		Intent it = getIntent();
		cod = it.getIntExtra("cod",1);
		
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		
		rs = bd.rawQuery("SELECT * FROM livros WHERE cod=?", new String[]{String.valueOf(cod)});
		
		foto = (ImageView) findViewById(R.id.img);
		isbn = (EditText) findViewById(R.id.isbn);
		titulo = (EditText) findViewById(R.id.titulo);
		subtitulo = (EditText) findViewById(R.id.subtitulo);
		edicao = (EditText) findViewById(R.id.edicao);
		autor = (EditText) findViewById(R.id.autor);
		npag = (EditText) findViewById(R.id.npag);
		ano = (EditText) findViewById(R.id.ano);
		editora = (EditText) findViewById(R.id.editora);
		salvar = (Button)findViewById(R.id.salvar);
		sair = (Button)findViewById(R.id.sair);
		altImg = (Button)findViewById(R.id.altimg);
		font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		
		categoria = (Spinner) findViewById(R.id.categoria);
		categoria.setOnItemSelectedListener(this);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.categoria_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoria.setAdapter(adapter);
		
		titulo.setTypeface(font);
		subtitulo.setTypeface(font);
		autor.setTypeface(font);
		isbn.setTypeface(font);
		editora.setTypeface(font);
		edicao.setTypeface(font);
		ano.setTypeface(font);
		npag.setTypeface(font);
		salvar.setTypeface(font);
		sair.setTypeface(font);
		altImg.setTypeface(font);
		
		
		if(rs.moveToNext()){
			String tit = rs.getString(rs.getColumnIndex("titulo"));
			imagem = rs.getBlob(rs.getColumnIndex("foto"));
			String subt = rs.getString(rs.getColumnIndex("subTitulo"));
			String aut = rs.getString(rs.getColumnIndex("autor"));
			String isb = rs.getString(rs.getColumnIndex("codIsbn"));
			String edit = rs.getString(rs.getColumnIndex("editora"));
			String edic = rs.getString(rs.getColumnIndex("edicao"));
			String year = rs.getString(rs.getColumnIndex("anoPublic"));
			String pags = rs.getString(rs.getColumnIndex("noPaginas"));
			
			titulo.setText(tit);
			subtitulo.setText(subt);
			autor.setText(aut);
			isbn.setText(isb);
			editora.setText(edit);
			edicao.setText(edic);
			ano.setText(year);
			npag.setText(pags);
			
			ByteArrayInputStream imageStream = new ByteArrayInputStream(imagem);
	        Bitmap imageBitmap= BitmapFactory.decodeStream(imageStream);
	        foto.setImageBitmap(imageBitmap);
	       
		}
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
		
		altImg.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	Intent it = new Intent(Intent.ACTION_PICK);
	    		it.setType("image/*");
	    		startActivityForResult(it,IMAGEM_INTERNA);
	        }
	    });
		salvar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(TextUtils.isEmpty(isbn.getText().toString())) {
	        		isbn.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(titulo.getText().toString())){
	        		titulo.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(edicao.getText().toString())){
	        		edicao.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(autor.getText().toString())){
	        		autor.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(npag.getText().toString())){
	        		npag.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(ano.getText().toString())){
	        		ano.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(editora.getText().toString())){
	        		editora.setError("Preencha este campo");
	        		return;
	        	}else if(i==0){
	        		imagem = rs.getBlob(rs.getColumnIndex("foto"));
	        		String tit = rs.getString(rs.getColumnIndex("titulo"));
		        	msgBox = new AlertDialog.Builder(EditLivro.this);
		            msgBox.setTitle("Confirmar");
		            msgBox.setIcon(android.R.drawable.ic_menu_info_details);
		            msgBox.setMessage("Tem certeza que quer alterar o livro \n "+tit+"?");
		            msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		                @Override
		                public void onClick(DialogInterface dialog, int which) {
		                	String tit = titulo.getText().toString();
		            		String subt = subtitulo.getText().toString();
		            		String aut = autor.getText().toString();
		            		String isb = isbn.getText().toString();
		            		String edit = editora.getText().toString();
		            		String edic = edicao.getText().toString();
		            		String cat = categoria.getSelectedItem().toString();
		            		
		            		int year = Integer.parseInt(String.valueOf(ano.getText().toString()));
		            		int pags = Integer.parseInt(String.valueOf(npag.getText().toString()));
		            		BD.updateLivro(bd, cod, tit, subt, aut,  isb, edit, edic, year, pags, cat);    
		                	
		                	Intent it = new Intent(EditLivro.this, Admin_catalogo.class);
		                	finish();
		                	startActivity(it);
		                }
		            });
		            msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		                @Override
		                public void onClick(DialogInterface dialog, int which) {
		                	Intent it = new Intent(EditLivro.this, Admin_catalogo.class);
		                	finish();
		                	startActivity(it);
		                }
		            });
		            msgBox.show();
	        		return;
	        	}else{      			
		        		ByteArrayOutputStream stream = new ByteArrayOutputStream();
	    	    		bitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);
	    	    		final byte[] img = stream.toByteArray();
	    	    		
	    	    		String tit = rs.getString(rs.getColumnIndex("titulo"));
	    	        	msgBox = new AlertDialog.Builder(EditLivro.this);
	    	            msgBox.setTitle("Confirmar");
	    	            msgBox.setIcon(android.R.drawable.ic_menu_info_details);
	    	            msgBox.setMessage("Tem certeza que quer alterar o livro \n "+tit+"?");
	    	            msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	    	                @Override
	    	                public void onClick(DialogInterface dialog, int which) {
	    	                	String tit = titulo.getText().toString();
	    	            		String subt = subtitulo.getText().toString();
	    	            		String aut = autor.getText().toString();
	    	            		String isb = isbn.getText().toString();
	    	            		String edit = editora.getText().toString();
	    	            		String edic = edicao.getText().toString();
	    	            		String cat = categoria.getSelectedItem().toString();
	    	            		
	    	            		int year = Integer.parseInt(String.valueOf(ano.getText().toString()));
	    	            		int pags = Integer.parseInt(String.valueOf(npag.getText().toString()));
	    	            		BD.updateLivro(bd, cod, img, tit, subt, aut,  isb, edit, edic, year, pags, cat);    	                	
	    	                	Intent it = new Intent(EditLivro.this, Admin_catalogo.class);
	    	                	finish();
	    	                	startActivity(it);
	    	                }
	    	            });
	    	            msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	    	                @Override
	    	                public void onClick(DialogInterface dialog, int which) {
	    	                	Intent it = new Intent(EditLivro.this, Admin_catalogo.class);
	    	                	finish();
	    	                	startActivity(it);
	    	                }
	    	            });
	    	            msgBox.show();
	        	}   
	        	
    		}
	    });
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent it){
		if(requestCode == IMAGEM_INTERNA){
			if(resultCode == RESULT_OK){
				Uri imagemSelecionada = it.getData();
				
				String[] colunas = {MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(imagemSelecionada, colunas, null, null, null);
				
				cursor.moveToFirst();
				
				int indexColuna = cursor.getColumnIndex(colunas[0]);
				String pathImg = cursor.getString(indexColuna);
				cursor.close();
				
				bitmap = BitmapFactory.decodeFile(pathImg);
				ImageView img = (ImageView) findViewById(R.id.img);
				img.setImageBitmap(bitmap);
				i++;
			}
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// TODO Auto-generated method stub
		parent.getItemAtPosition(pos);
    	cat = parent.getItemAtPosition(pos).toString();
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
