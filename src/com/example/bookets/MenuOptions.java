package com.example.bookets;

public class MenuOptions {
	private String login;
	private String nome;
	private int idPhoto;

	public int getIdPhoto() {
		return idPhoto;
	}

	public void setIdPhoto(int idPhoto) {
		this.idPhoto = idPhoto;
	}

	public MenuOptions() {
	}

	public MenuOptions(String login) {
		this.login = login;
	}

	public MenuOptions(String login, String nome) {
		this.login = login;
		this.nome = nome;
	}

	public MenuOptions(String login, String nome, int id) {
		this.login = login;
		this.nome = nome;
		this.idPhoto = id;
	}

	public String getlogin() {
		return login;
	}

	public void setlogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return login + " - " + nome + "\n";
	}
}
