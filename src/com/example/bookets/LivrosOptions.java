package com.example.bookets;

public class LivrosOptions {
	private int cod;
	private String titulo;
	private String subtitulo;
	private byte[] foto;
	private String autor;
	private String isbn;
	private String editora;
	private String edicao;
	private int ano;
	private int pags;
	private String categoria;
	
	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubtitulo() {
		return subtitulo;
	}

	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public String getEdicao() {
		return edicao;
	}

	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getPags() {
		return pags;
	}

	public void setPags(int pags) {
		this.pags = pags;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto2) {
		this.foto = foto2;
	}

	public LivrosOptions() {
	}

	public LivrosOptions(String titulo,	String subtitulo,byte[] foto,String autor,String isbn,String editora,String edicao,int ano,int pags,String categoria) {
		 this.titulo = titulo;
		 this.subtitulo = subtitulo;
		 this.foto = foto;
		 this.autor = autor;
		 this.isbn = isbn;
		 this.editora = editora;
		 this.edicao = edicao;
		 this.ano = ano;
		 this.pags = pags;
		 this.categoria = categoria;
	}
	public LivrosOptions(int cod, String titulo, String subtitulo,byte[] foto,String autor) {
		 this.cod = cod;
		 this.titulo = titulo;
		 this.subtitulo = subtitulo;
		 this.foto = foto;
		 this.autor = autor;
	}
	

	public LivrosOptions(String titulo,	String subtitulo,byte[] foto,String autor) {
		 this.titulo = titulo;
		 this.subtitulo = subtitulo;
		 this.foto = foto;
		 this.autor = autor;
	}
}
