package com.example.bookets;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BD extends SQLiteOpenHelper{
	
	public static SQLiteDatabase db;
	private static Context context;
	
	public BD(Context context, String dbName, int version) {
		super(context, dbName, null, version);
		this.context = context.getApplicationContext();
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		String tb_livro = "CREATE TABLE IF NOT EXISTS livros(" +
				"cod INTEGER PRIMARY KEY AUTOINCREMENT," +
				"foto BLOB," +
				"titulo VARCHAR(60) NOT NULL," +
				"subTitulo VARCHAR(70)," +
				"autor VARCHAR(30)," +
				"codIsbn VARCHAR(13)," +
				"editora VARCHAR(30)," +
				"edicao VARCHAR(20)," +
				"anoPublic INTEGER," +
				"noPaginas INTEGER," +
				"categoria VARCHAR(30));";
		db.execSQL(tb_livro);
		
		String tb_users = 
				"CREATE TABLE IF NOT EXISTS users("+
					"id INTEGER PRIMARY KEY AUTOINCREMENT," +
					"nome VARCHAR(30) NOT NULL," +
					"login VARCHAR(30) UNIQUE NOT NULL," +
					"senha VARCHAR(30) NOT NULL," +
					"tipo INT(1) NOT NULL);";
		db.execSQL(tb_users);
		
		addAdmin(db,"Iskander","iskander","admin",1);
		addAdmin(db,"Andre","andre","admin",1);
		addAdmin(db,"Gabriel","gabriel","admin",1);
		addAdmin(db,"Victor","victor","admin",1);
		addAdmin(db,"Renan","renan","admin",1);
		
		addAdmin(db,"teste","teste","teste",0);
		
		addLivro(db, setImage(R.drawable.livro), "Netinho de paula","Como eu batia na minha mulher","Clodovil Hernandes","8798795616545","Sextante","Primeira",2015,1000,"Biografia");
		addLivro(db, setImage(R.drawable.livro3), "Leonardo da Vinci","","Walter Isaacson","9788551002575","Intrinseca","Primeira",2017,640,"Biografia");
		addLivro(db, setImage(R.drawable.livro1), "Origem","","Dan Brown","858041766X","Arqueiro","Primeira",2017,432,"Lit. Estrangeira");
		addLivro(db, setImage(R.drawable.livro2), "A Elite do Atraso","","Jesse Souza","8544105378","Leya Casa da Palavra","Primeira",2017,240,"Política");
		addLivro(db, setImage(R.drawable.livro6), "O Poder do Habito","","Charles Duhigg","8539004119","Objetiva","Primeira",2012,408,"Ciência");
		addLivro(db, setImage(R.drawable.livro5), "Empreendedorismo Para Subversivos","Um Guia para abrir seu negocio no pós-capitalismo","Facundo Guerra","8539004119","Estrategia","Primeira",2017,240,"Ciência");
		addLivro(db, setImage(R.drawable.livro9), "Rita Lee - Uma Autobiografia","","Rita Lee","8525063304","Globo Livros","Primeira",2016,296,"Biografia");
		addLivro(db, setImage(R.drawable.livro4), "Outros Jeitos de Usar a Boca","","Rupi Kaur","884542209303","Planeta Brasil","Primeira",2017,208,"Lit. Estrangeira");
		addLivro(db, setImage(R.drawable.livro8), "Caminho Suave - Cartilha - Ensino Fundamental I - Integrado","Afabetização Pela Imagem","Branca Alves de Lima","8589987329","Caminho Suave ED","No 123",2015,128,"Vestibular");
		addLivro(db, setImage(R.drawable.livro7), "Atenção Plena - Mindfulness - Como Encontrar A Paz Em um Mundo Frenetico","Em Um Mundo Frenetico","Mark Williams","8543101875","Sextante","Primeira",2015,208,"Auto-Ajuda");	
		
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
	public static void addLivro(SQLiteDatabase db, byte[] foto, String titulo, String subtitulo, String autor, String isbn, String editora, String edicao, int ano, int pags, String categoria) { 	   
			ContentValues values = new ContentValues();
			values = new ContentValues();
			
			values.put("foto", foto);
			values.put("titulo", titulo);
			values.put("subTitulo", subtitulo);
			values.put("autor", autor);
			values.put("codIsbn", isbn);
			values.put("editora", editora);
			values.put("edicao", edicao);
			values.put("anoPublic", ano);
			values.put("noPaginas", pags);
			values.put("categoria", categoria);
			
			db.insert("livros", "foto", values);
	}
	
	public static void updateLivro(SQLiteDatabase db,int cod, byte[] foto, String titulo, String subtitulo, String autor, String isbn, String editora, String edicao, int ano, int pags, String categoria) { 	   
		ContentValues values = new ContentValues();
		values = new ContentValues();
		
		values.put("foto", foto);
		values.put("titulo", titulo);
		values.put("subTitulo", subtitulo);
		values.put("autor", autor);
		values.put("codIsbn", isbn);
		values.put("editora", editora);
		values.put("edicao", edicao);
		values.put("anoPublic", ano);
		values.put("noPaginas", pags);
		values.put("categoria", categoria);
		
		String where = "cod = ?";

        String argumentos[] = {""+cod};
		
		db.update("livros", values, where, argumentos);
	}
	
	public static void updateLivro(SQLiteDatabase db,int cod, String titulo, String subtitulo, String autor, String isbn, String editora, String edicao, int ano, int pags, String categoria) { 	   
		ContentValues values = new ContentValues();
		values = new ContentValues();
		
		values.put("titulo", titulo);
		values.put("subTitulo", subtitulo);
		values.put("autor", autor);
		values.put("codIsbn", isbn);
		values.put("editora", editora);
		values.put("edicao", edicao);
		values.put("anoPublic", ano);
		values.put("noPaginas", pags);
		values.put("categoria", categoria);
		
		String where = "cod = ?";

        String argumentos[] = {""+cod};
		
		db.update("livros", values, where, argumentos);
	}
	
	public static byte[] setImage(int livro){
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), livro);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] imageInByte = stream.toByteArray();
		return imageInByte;
	}
	
	/*
	 * bang da imagem
	public static void exibirImg(int id){
		// consultaaobancode dados
		DataBaseHandler db = new DataBaseHandler(this);
		Contatocontato= db.getContato(id);
		byte[] outImage=contato.getFoto();
		ByteArrayInputStreamimageStream = new ByteArrayInputStream(outImage);
		Bitmap imageBitmap= BitmapFactory.decodeStream(imageStream);
		imagem.setImageBitmap(imageBitmap);
	}
	*/
	
	public static void addAdmin(SQLiteDatabase db, String nome, String login, String senha, int tipo){
		ContentValues values = new ContentValues();
		values = new ContentValues();

		values.put("nome", nome);
		values.put("login", login);
		values.put("senha", senha);
		values.put("tipo", tipo);
		db.insert("users", "nome", values);
	}
}
