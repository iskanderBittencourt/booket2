package com.example.bookets;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Entrada_menu_admin extends Activity {
	
	public ListView menu;
	public ArrayList<MenuOptions> menuList;
	public MenuAdapter adapter;
	String l;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entrada_menu);
		
		menu = (ListView) findViewById(R.id.menu);
		Intent it = getIntent();
		l = it.getStringExtra("login");
		
		menuList = new ArrayList<MenuOptions>();
		menuList.add(new MenuOptions("1","Catalogo de livros", R.drawable.ic_book));
		menuList.add(new MenuOptions("2","Editar Usu�rio", R.drawable.ic_edit));
		menuList.add(new MenuOptions("3","Sobre n�s", R.drawable.ic_person));
		menuList.add(new MenuOptions("4","Exit", R.drawable.ic_exit));
		menuList.add(new MenuOptions("5","Area administrativa", R.drawable.ic_settings));
		
		adapter = new MenuAdapter(this, menuList);
		menu.setAdapter(adapter);
		menu.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {  		
            	if(position==0){
    				Intent it = new Intent(Entrada_menu_admin.this,Catalogo.class);
    				startActivity(it);
	    		}else if(position==1){
	    			Intent it = new Intent(Entrada_menu_admin.this,EditUser.class);
	    			it.putExtra("login", l);
    				startActivity(it);
	    		}else if(position==2){
	    			Intent it = new Intent(Entrada_menu_admin.this,Equipe.class);
    				startActivity(it);	
	    		}else if(position==3){
	    			finish();
	    		}else{
	    			Intent it = new Intent(Entrada_menu_admin.this,Admin_catalogo.class);
    				startActivity(it);	
	    		}
            }
        });
	}
}
