package com.example.bookets;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class Novo_livro extends Activity implements OnItemSelectedListener{
	public static final int IMAGEM_INTERNA = 4;
	private static final int GALLERY_REQUEST = 4;
	public EditText isbn;
	public EditText titulo;
	public EditText subtitulo;
	public EditText edicao;
	public EditText autor;
	public EditText npag;
	public EditText ano;
	public EditText editora;
	public Button sair;
	public Button salvar;
	public Button altImg;
	public Spinner categoria;
	private SQLiteDatabase bd;
	public String cat;
	public AlertDialog.Builder msgBox;
	private Bitmap bitmap;
	public Typeface font;
	public Cursor rs;
	public int i= 0;
	public ImageView imagem;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_novo_livro);
		
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		
		isbn = (EditText) findViewById(R.id.isbn);
		titulo = (EditText) findViewById(R.id.titulo);
		subtitulo = (EditText) findViewById(R.id.subtitulo);
		edicao = (EditText) findViewById(R.id.edicao);
		autor = (EditText) findViewById(R.id.autor);
		npag = (EditText) findViewById(R.id.npag);
		ano = (EditText) findViewById(R.id.ano);
		editora = (EditText) findViewById(R.id.editora);
		salvar = (Button)findViewById(R.id.salvar);
		sair = (Button)findViewById(R.id.sair);
		altImg = (Button)findViewById(R.id.altimg);
		imagem = (ImageView) findViewById(R.id.img);
		font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		
		categoria = (Spinner) findViewById(R.id.categoria);
		categoria.setOnItemSelectedListener(this);
		
		titulo.setTypeface(font);
		subtitulo.setTypeface(font);
		autor.setTypeface(font);
		isbn.setTypeface(font);
		editora.setTypeface(font);
		edicao.setTypeface(font);
		ano.setTypeface(font);
		npag.setTypeface(font);
		salvar.setTypeface(font);
		sair.setTypeface(font);
		altImg.setTypeface(font);
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.categoria_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoria.setAdapter(adapter);
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
		
		altImg.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	Intent it = new Intent(Intent.ACTION_PICK);
	        	it.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());   
	    		it.setType("image/*");
	    		startActivityForResult(it,IMAGEM_INTERNA);
	        }
	    });
		salvar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(TextUtils.isEmpty(isbn.getText().toString())) {
	        		isbn.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(titulo.getText().toString())){
	        		titulo.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(edicao.getText().toString())){
	        		edicao.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(autor.getText().toString())){
	        		autor.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(npag.getText().toString())){
	        		npag.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(ano.getText().toString())){
	        		ano.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(editora.getText().toString())){
	        		editora.setError("Preencha este campo");
	        		return;
	        	}else if(i==0){
	        		Toast.makeText(Novo_livro.this, "Adicione uma imagem", Toast.LENGTH_LONG).show();
		        	return;
		        }else{
	        		String isb = isbn.getText().toString();
	        		rs = bd.rawQuery("SELECT * FROM livros WHERE codIsbn=?", new String[]{isb});
	        		if(rs.moveToNext()){
	        			Toast.makeText(Novo_livro.this, "Codigo ISBN j� existente", Toast.LENGTH_LONG).show();
	        		}else{
	        			String tit = titulo.getText().toString();
	    	    		ByteArrayOutputStream stream = new ByteArrayOutputStream();
	    	    		bitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);
	    	    		final byte[] img = stream.toByteArray();
	    	    		
	    	    		msgBox = new AlertDialog.Builder(Novo_livro.this);
	    	            msgBox.setTitle("Confirmar");
	    	            msgBox.setIcon(android.R.drawable.ic_menu_info_details);
	    	            msgBox.setMessage("Tem certeza que quer inserir o livro \n "+tit+"?");
	    	            msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	    	                @Override
	    	                public void onClick(DialogInterface dialog, int which) {
	    	                	String tit = titulo.getText().toString();
	    	            		String subt = subtitulo.getText().toString();
	    	            		String aut = autor.getText().toString();
	    	            		String isb = isbn.getText().toString();
	    	            		String edit = editora.getText().toString();
	    	            		String edic = edicao.getText().toString();
	    	            		
	    	            		int year = Integer.parseInt(String.valueOf(ano.getText().toString()));
	    	            		int pags = Integer.parseInt(String.valueOf(npag.getText().toString()));
	    	                	BD.addLivro(bd, img, tit, subt, aut, isb, edit, edic, year, pags, cat);
	    	                	
	    	                	Intent it = new Intent(Novo_livro.this,Admin_catalogo.class);
	    	                	finish();
	    	                	startActivity(it);
	    	                }
	    	            });
	    	            msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	    	                @Override
	    	                public void onClick(DialogInterface dialog, int which) {
	    	                	finish();
	    	                }
	    	            });
	    	            msgBox.show();
	        		}
	        		
	        	}      	
	        }
	    });
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent it){
		if(requestCode == IMAGEM_INTERNA){
			if(resultCode == RESULT_OK){
				Uri imagemSelecionada = it.getData();
				
				String[] colunas = {MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(imagemSelecionada, colunas, null, null, null);
				
				cursor.moveToFirst();
				
				int indexColuna = cursor.getColumnIndex(colunas[0]);
				String pathImg = cursor.getString(indexColuna);
				cursor.close();
				
				bitmap = BitmapFactory.decodeFile(pathImg);
				ImageView img = (ImageView) findViewById(R.id.img);
				img.setImageBitmap(bitmap);
				i++;
			}
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// TODO Auto-generated method stub
		parent.getItemAtPosition(pos);
    	cat = parent.getItemAtPosition(pos).toString();
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}