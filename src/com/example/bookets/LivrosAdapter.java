package com.example.bookets;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LivrosAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<LivrosOptions> LivrosList;
	
	public LivrosAdapter(Context context, ArrayList<LivrosOptions> LivrosList){
		this.context = context;
		this.LivrosList = LivrosList;
	}
	@Override
	public int getCount() {
		return LivrosList.size();
	}

	@Override
	public Object getItem(int position) {
		return LivrosList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LivrosOptions opcao_atual = LivrosList.get(position);
		View item = LayoutInflater.from(context).inflate(R.layout.livros_option, null);
		
		ImageView img;
		TextView titulo;
		TextView sub;
		TextView autor;
		
		img = (ImageView) item.findViewById(R.id.foto);
		titulo = (TextView) item.findViewById(R.id.titulo);
		sub = (TextView) item.findViewById(R.id.sub);
		autor = (TextView) item.findViewById(R.id.autor);
		
		titulo.setText(opcao_atual.getTitulo());
		sub.setText(opcao_atual.getSubtitulo());
		autor.setText(opcao_atual.getAutor());
		
		byte[] image = opcao_atual.getFoto();
		ByteArrayInputStream imageStream = new ByteArrayInputStream(image);
        Bitmap imageBitmap= BitmapFactory.decodeStream(imageStream);
        img.setImageBitmap(imageBitmap);
		
		return item;
	}

}
