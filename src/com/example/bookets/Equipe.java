package com.example.bookets;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Equipe extends Activity {
	
	Typeface font;
	TextView nG;
	TextView eG;
	TextView rG;
	
	TextView nI;
	TextView eI;
	TextView rI;
	
	TextView nV;
	TextView eV;
	TextView rV;
	
	TextView nR;
	TextView eR;
	TextView rR;
	
	TextView nA;
	TextView eA;
	TextView rA;
	
	Button sair;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_equipe);
		
		nG = (TextView)findViewById(R.id.txtGab1);
		eG = (TextView)findViewById(R.id.txtGab2);
		rG = (TextView)findViewById(R.id.txtGab3);
		
		nI = (TextView)findViewById(R.id.txtIkki1);
		eI = (TextView)findViewById(R.id.txtIkki2);
		rI = (TextView)findViewById(R.id.txtIkki3);
		
		nV = (TextView)findViewById(R.id.txtVic1);
		eV = (TextView)findViewById(R.id.txtVic2);
		rV = (TextView)findViewById(R.id.txtVic3);
		
		nR = (TextView)findViewById(R.id.txtRen1);
		eR = (TextView)findViewById(R.id.txtRen2);
		rR = (TextView)findViewById(R.id.txtRen3);
		
		nA = (TextView)findViewById(R.id.txtAnd1);
		eA = (TextView)findViewById(R.id.txtAnd2);
		rA = (TextView)findViewById(R.id.txtAnd3);
		
		sair = (Button)findViewById(R.id.sair);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		
		nG.setTypeface(font);
		eG.setTypeface(font);
		rG.setTypeface(font);
		nI.setTypeface(font);
		eI.setTypeface(font);
		rI.setTypeface(font);  
		nV.setTypeface(font);
		eV.setTypeface(font);
		rV.setTypeface(font);  
		nR.setTypeface(font);
		eR.setTypeface(font);
		rR.setTypeface(font);  
		nA.setTypeface(font);
		eA.setTypeface(font);
		rA.setTypeface(font);
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}
}
