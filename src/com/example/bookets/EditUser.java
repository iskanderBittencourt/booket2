package com.example.bookets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditUser extends Activity {
	private EditText login;
	private EditText nome;
	private EditText confSenha;
	private EditText senha;
	private TextView ola;
	private Button salvar;
	private Button sair;
	public SQLiteDatabase bd;
	public AlertDialog.Builder msgBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_user);
		
		ola = (TextView) findViewById(R.id.ola);
		login = (EditText) findViewById(R.id.login);
		nome = (EditText) findViewById(R.id.nome);
		senha = (EditText) findViewById(R.id.senha);
		confSenha = (EditText) findViewById(R.id.confSenha);
		salvar = (Button) findViewById(R.id.salvar);
		sair = (Button) findViewById(R.id.sair);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		Intent it = getIntent();
		String l = it.getStringExtra("login");
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		
		ola.setTypeface(font);
		login.setTypeface(font);
		nome.setTypeface(font);
		senha.setTypeface(font);
		confSenha.setTypeface(font);
		salvar.setTypeface(font);
		sair.setTypeface(font);
	
		Cursor rs = bd.rawQuery("SELECT * FROM users WHERE login=?", new String[]{l});
      	
    	if(rs.moveToNext()){
    		login.setText(rs.getString(2));
    		ola.setText("Ol� " + rs.getString(1) + " deseja alterar alguma coisa?");
    	}
    	
    	salvar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(TextUtils.isEmpty(senha.getText().toString())) {
	        		senha.setError("Preencha este campo");
	        		return;
	        	}else if(TextUtils.isEmpty(confSenha.getText().toString())){
	        		confSenha.setError("Preencha este campo");
	        		return;
	        	}else if(!senha.getText().toString().equals(confSenha.getText().toString())){
	        		Toast.makeText(EditUser.this, "Senhas n�o conferem", Toast.LENGTH_LONG).show();
	        	}else{
	        		if(nome.getText().toString().equals("")){
		        		msgBox = new AlertDialog.Builder(EditUser.this);
		    			msgBox.setTitle("Confirmar");
		    			msgBox.setIcon(android.R.drawable.ic_menu_info_details);
		    			msgBox.setMessage("Salvar altera��es?");
		    			msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					String l = login.getText().toString();
		    					String s =  senha.getText().toString();
		    					bd.execSQL("UPDATE users SET senha=? WHERE login=?",new String[]{s, l});
		    					Toast.makeText(EditUser.this, "Altera��es salvas", Toast.LENGTH_LONG).show();
		    					finish();
		    				}
		    			});
		    			msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					nome.setText("");
		    					senha.setText("");
		    					confSenha.setText("");
		    				}
		    			});
		    			msgBox.show();
		        	}else {
		        		msgBox = new AlertDialog.Builder(EditUser.this);
		    			msgBox.setTitle("Confirmar");
		    			msgBox.setIcon(android.R.drawable.ic_menu_info_details);
		    			msgBox.setMessage("Salvar altera��es?");
		    			msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					String l = login.getText().toString();
		    					String s =  senha.getText().toString();
		    					String n =  nome.getText().toString();
		    					bd.execSQL("UPDATE users SET nome=?, senha=? WHERE login=?",new String[]{n, s, l});
		    					Toast.makeText(EditUser.this, "Altera��es salvas", Toast.LENGTH_LONG).show();
		    				}
		    			});
		    			msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					nome.setText("");
		    					senha.setText("");
		    					confSenha.setText("");
		    				}
		    			});
		    			msgBox.show();
		        	}
	        	}
	        }
	    });
    	
    	sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	finish();
	        }
	    });
	}
}
