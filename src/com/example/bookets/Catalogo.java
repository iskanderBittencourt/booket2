package com.example.bookets;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

public class Catalogo extends Activity implements OnItemClickListener{
	
	public ListView catalogo;
	public EditText buscar;
	public ImageView ok;
	public SQLiteDatabase bd;
	public Button novo;
	public Button sair;
	public Cursor rs;
	public ArrayList<LivrosOptions> livrosList;
	public LivrosAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_catalogo);
		
		livrosList = new ArrayList<LivrosOptions>();
		catalogo = (ListView)findViewById(R.id.catalogo);
		buscar = (EditText)findViewById(R.id.busca);
		ok = (ImageView)findViewById(R.id.ok);
		sair = (Button)findViewById(R.id.sair);
		novo = (Button)findViewById(R.id.novo);
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		
		rs = bd.rawQuery("SELECT * FROM livros", null);
		catalogo.setOnItemClickListener(this);
		sair.setTypeface(font);
		
		buscar.addTextChangedListener(new TextWatcher() {
		    @Override
		    public void afterTextChanged(Editable s) {
		        // TODO Auto-generated method stub
		    }

		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		        // TODO Auto-generated method stub
		    }

		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    	livrosList.clear();
		    	String busca = buscar.getText().toString();
		    	rs = bd.rawQuery("SELECT * FROM livros WHERE titulo LIKE ? OR autor LIKE ? OR categoria LIKE ? ORDER BY titulo DESC", new String[]{"%"+busca+"%","%"+busca+"%","%"+busca+"%"});
		    	if(rs.moveToNext()){
		    		String t = rs.getString(rs.getColumnIndex("titulo"));
					String sub = rs.getString(rs.getColumnIndex("subTitulo"));
					String a = rs.getString(rs.getColumnIndex("autor"));
					byte[] img = rs.getBlob(rs.getColumnIndex("foto"));
					int c = rs.getInt(rs.getColumnIndex("cod"));
					
					livrosList.add(new LivrosOptions(c,t,sub,img,a));
					while(rs.moveToNext()){
						t = rs.getString(rs.getColumnIndex("titulo"));
						sub = rs.getString(rs.getColumnIndex("subTitulo"));
						a = rs.getString(rs.getColumnIndex("autor"));
						img = rs.getBlob(rs.getColumnIndex("foto"));
						c = rs.getInt(rs.getColumnIndex("cod"));
						
						livrosList.add(new LivrosOptions(c,t,sub,img,a));
					}
					adapter = new LivrosAdapter(Catalogo.this, livrosList);
					catalogo.setAdapter(adapter);
				}else{
					String[] values= {"Nenhum livro achado :("};
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(Catalogo.this, android.R.layout.simple_list_item_1, values);
					catalogo.setAdapter(adapter);
				}
		    } 
		});
		
		if(rs.moveToNext()){
			String t = rs.getString(rs.getColumnIndex("titulo"));
			String sub = rs.getString(rs.getColumnIndex("subTitulo"));
			String a = rs.getString(rs.getColumnIndex("autor"));
			byte[] img = rs.getBlob(rs.getColumnIndex("foto"));
			int c = rs.getInt(rs.getColumnIndex("cod"));
			
			livrosList.add(new LivrosOptions(c,t,sub,img,a));
			while(rs.moveToNext()){
				t = rs.getString(rs.getColumnIndex("titulo"));
				sub = rs.getString(rs.getColumnIndex("subTitulo"));
				a = rs.getString(rs.getColumnIndex("autor"));
				img = rs.getBlob(rs.getColumnIndex("foto"));
				c = rs.getInt(rs.getColumnIndex("cod"));
				
				livrosList.add(new LivrosOptions(c,t,sub,img,a));
			}
			adapter = new LivrosAdapter(Catalogo.this, livrosList);
			catalogo.setAdapter(adapter);
		}
         
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		LivrosOptions itemClicado = (LivrosOptions) parent.getItemAtPosition(position);
		Intent it = new Intent(this, Detalhes.class);
		it.putExtra("cod", itemClicado.getCod());
		startActivity(it);
	}	
}
