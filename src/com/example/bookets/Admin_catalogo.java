package com.example.bookets;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class Admin_catalogo extends Activity implements OnItemClickListener, OnItemLongClickListener{
	
	public ListView catalogo;
	public EditText buscar;
	public ImageView ok;
	public SQLiteDatabase bd;
	public Button novo;
	public Button sair;
	int i;
	int count;
	public Cursor rs;
	public ArrayList<LivrosOptions> livrosList;
	public LivrosAdapter adapter;
	public AlertDialog.Builder msgBox;
	LivrosOptions itemClicado;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admin_catalogo);
		
		livrosList = new ArrayList<LivrosOptions>();
		catalogo = (ListView)findViewById(R.id.catalogo);
		buscar = (EditText)findViewById(R.id.busca);
		ok = (ImageView)findViewById(R.id.ok);
		sair = (Button)findViewById(R.id.sair);
		novo = (Button)findViewById(R.id.novo);
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		rs = bd.rawQuery("SELECT * FROM livros", null);
		
		catalogo.setOnItemClickListener(this);
		catalogo.setOnItemLongClickListener(this);		
		
		buscar.addTextChangedListener(new TextWatcher() {
		    @Override
		    public void afterTextChanged(Editable s) {
		        // TODO Auto-generated method stub
		    }

		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		        // TODO Auto-generated method stub
		    }

		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		    	livrosList.clear();
		    	String busca = buscar.getText().toString();
		    	rs = bd.rawQuery("SELECT * FROM livros WHERE titulo LIKE ? OR autor LIKE ? OR categoria LIKE ? ORDER BY titulo DESC", new String[]{"%"+busca+"%","%"+busca+"%","%"+busca+"%"});
		    	
				if(rs.moveToNext()){
					String t = rs.getString(rs.getColumnIndex("titulo"));
					String sub = rs.getString(rs.getColumnIndex("subTitulo"));
					String a = rs.getString(rs.getColumnIndex("autor"));
					byte[] img = rs.getBlob(rs.getColumnIndex("foto"));
					int c = rs.getInt(rs.getColumnIndex("cod"));
					
		            livrosList.add(new LivrosOptions(c, t,sub,img,a));
					while(rs.moveToNext()){
						t = rs.getString(rs.getColumnIndex("titulo"));
						sub = rs.getString(rs.getColumnIndex("subTitulo"));
						a = rs.getString(rs.getColumnIndex("autor"));
						img = rs.getBlob(rs.getColumnIndex("foto"));
						c = rs.getInt(rs.getColumnIndex("cod"));
						
			            livrosList.add(new LivrosOptions(c, t,sub,img,a));
					}
					adapter = new LivrosAdapter(Admin_catalogo.this, livrosList);
					catalogo.setAdapter(adapter);
				}else{
					String[] values= {"Nenhum livro achado :("};
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(Admin_catalogo.this, android.R.layout.simple_list_item_1, values);
					catalogo.setAdapter(adapter);
				}
		    } 
		});
		
		if(rs.moveToNext()){
			String t = rs.getString(rs.getColumnIndex("titulo"));
			String sub = rs.getString(rs.getColumnIndex("subTitulo"));
			String a = rs.getString(rs.getColumnIndex("autor"));
			byte[] img = rs.getBlob(rs.getColumnIndex("foto"));
			int c = rs.getInt(rs.getColumnIndex("cod"));
			
            livrosList.add(new LivrosOptions(c, t,sub, img,a));
			while(rs.moveToNext()){
				c = rs.getInt(rs.getColumnIndex("cod"));
				t = rs.getString(rs.getColumnIndex("titulo"));
				sub = rs.getString(rs.getColumnIndex("subTitulo"));
				a = rs.getString(rs.getColumnIndex("autor"));
				img = rs.getBlob(rs.getColumnIndex("foto"));
				
	            livrosList.add(new LivrosOptions(c, t,sub, img,a));
			}
			adapter = new LivrosAdapter(Admin_catalogo.this, livrosList);
			catalogo.setAdapter(adapter);
		}else{
			String[] values= {"Nenhum livro achado :("};
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(Admin_catalogo.this, android.R.layout.simple_list_item_1, values);
			catalogo.setAdapter(adapter);
		}
		
		novo.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            Intent it = new Intent(Admin_catalogo.this, Novo_livro.class);
	            finish();
	            startActivity(it);
	        }
	    });
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		itemClicado = (LivrosOptions) parent.getItemAtPosition(position);
		rs = bd.rawQuery("SELECT * FROM livros WHERE cod=?",new String[]{""+itemClicado.getCod()});
		if(rs.moveToNext()){
			final int c = rs.getInt(0);
			String nome = rs.getString(2);
			msgBox = new AlertDialog.Builder(Admin_catalogo.this);
			msgBox.setTitle("Confirmar");
			msgBox.setIcon(android.R.drawable.ic_menu_info_details);
			msgBox.setMessage("Tem certeza que quer apagar o livro \n "+nome+"?");
			msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					bd.execSQL("DELETE FROM livros WHERE cod=?", new String[]{"" + c});
					livrosList.remove(itemClicado);
					adapter.notifyDataSetChanged();
				}
			});
			msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
			msgBox.show();
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		LivrosOptions itemClicado = (LivrosOptions) parent.getItemAtPosition(position);
		Intent it = new Intent(Admin_catalogo.this, EditLivro.class);
		it.putExtra("cod", itemClicado.getCod());
		finish();
		startActivity(it);
	}	
}
