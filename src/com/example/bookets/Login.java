package com.example.bookets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends Activity {
	
	private EditText login;
	private EditText senha;
	private TextView esqueceu;
	private Button logar;
	private Button sair;
	public SQLiteDatabase bd;
	public AlertDialog.Builder msgBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		esqueceu = (TextView) findViewById(R.id.esqueceu);
		login = (EditText) findViewById(R.id.login);
		senha = (EditText) findViewById(R.id.senha);
		logar = (Button) findViewById(R.id.entrar);
		sair = (Button) findViewById(R.id.sair);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa.ttf");
		bd = new BD(this, "bookets", 1).getWritableDatabase();
		
		esqueceu.setTypeface(font);
		logar.setTypeface(font);
		sair.setTypeface(font);
		
		
		esqueceu.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            Intent it = new Intent(Login.this,Esqueceu_senha.class);
	            startActivity(it);
	        }
	    });
		
		logar.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	String L = login.getText().toString();
	        	String S = senha.getText().toString();
	        	
	        	Cursor rs = bd.rawQuery("SELECT * FROM users WHERE login=?", new String[]{L});
	   
	        	
	        	if(rs.moveToNext()){
	        		rs = bd.rawQuery("SELECT * FROM users WHERE login=? AND senha=?", new String[]{L,S});
	        		if(rs.moveToNext()){
	        			rs = bd.rawQuery("SELECT * FROM users WHERE login=? AND senha=? AND tipo=1", new String[]{L,S});
	        			if(rs.moveToNext()){
	        				Intent it = new Intent(Login.this, Entrada_menu_admin.class);
	        				it.putExtra("login", login.getText().toString());
			        		startActivity(it);
	        			}else{
	        				Intent it = new Intent(Login.this, Entrada_menu.class);
	        				it.putExtra("login", login.getText().toString());
			        		startActivity(it);
	        			}
	        			
	        		}else{
	        			msgBox = new AlertDialog.Builder(Login.this);
		    			msgBox.setTitle("Esqueceu a senha?");
		    			msgBox.setIcon(android.R.drawable.ic_menu_info_details);
		    			msgBox.setMessage("A senha que digitou n�o confere, gostaria de lembra-la?");
		    			msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					Intent it = new Intent(Login.this,Esqueceu_senha.class);
		    					it.putExtra("login", login.getText().toString());
		    		            startActivity(it);	
		    				}
		    			});
		    			msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		    				@Override
		    				public void onClick(DialogInterface dialog, int which) {
		    					senha.setText("");
		    					senha.requestFocus();
		    				}
		    			});
		    			msgBox.show();
	        		}	
	    		}else{
	    			msgBox = new AlertDialog.Builder(Login.this);
	    			msgBox.setTitle("Usu�rio Inexistente");
	    			msgBox.setIcon(android.R.drawable.ic_menu_info_details);
	    			msgBox.setMessage("Este usu�rio n�o existe gostaria de criar um novo?");
	    			msgBox.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	    				@Override
	    				public void onClick(DialogInterface dialog, int which) {
	    					Intent it = new Intent(Login.this,Cadastro.class);
	    					it.putExtra("login", login.getText().toString());
	    					it.putExtra("senha", senha.getText().toString());
	    		            startActivity(it);	
	    				}
	    			});
	    			msgBox.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
	    				@Override
	    				public void onClick(DialogInterface dialog, int which) {
	    					login.setText("");
	    					senha.setText("");
	    					login.requestFocus();
	    				}
	    			});
	    			msgBox.show();
	    			
	    		}
	        }
	    });
		
		sair.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            finish();
	        }
	    });
	}

}
