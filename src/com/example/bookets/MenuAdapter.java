package com.example.bookets;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<MenuOptions> menuList;
	
	public MenuAdapter(Context context, ArrayList<MenuOptions> menuList){
		this.context = context;
		this.menuList = menuList;
	}
	
	@Override
	public int getCount() {
		return menuList.size();
	}

	@Override
	public Object getItem(int position) {
		return menuList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuOptions opcao_atual = menuList.get(position);
		View item = LayoutInflater.from(context).inflate(R.layout.menu_option, null);
		
		ImageView img;
		TextView option;
		
		img = (ImageView) item.findViewById(R.id.foto);
		option = (TextView) item.findViewById(R.id.opcao);
		
		img.setImageResource(opcao_atual.getIdPhoto());
		option.setText(opcao_atual.getNome());
		
		return item;
	}

}
